// Seegal Panchal
// Feb 6th 2018
// This functions counts the arguments passed to the program

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "usage:%s,[num1, num2, ...]\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("argc: %d\targs: ", argc-1);
        int i;
        for (i = 1; i < argc; i++)
        {
            if (i == argc-1) printf("%s\n", argv[i]);
            else printf("%s, ", argv[i]);
        }
    }
    return 0;
}