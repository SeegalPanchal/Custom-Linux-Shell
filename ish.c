// Seegal Panchal
// Wed Feb 6th

#include <pwd.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/signal.h>

extern char **getln();
//const char *mypath[] = { "./", "/usr/bin/", "/bin/", NULL };

//signatures
void prompt();
void sig_child_handler(int sigint);
void input_from_file(int len, char **args);
void output_to_file(int len, char **args);
void set_flags(char **args, int *len, int *backgroundProcess, int *fileIn, int *fileOut);

// Argument Counter
int argument_counter(int argc, char **argv);

// GCD Signatures
int gcd(int argc, char **argv);
long calculate_gcd(long num1, long num2);
long parse_number(char *num, int *error);

// Prime Factor
int prime_factor(int argc, char **argv);
void calculate_factors(int n);
int is_prime(int num);
int check_number(char *num, int *error);


/* -------------------------------- MAIN --------------------------------------------*/

int main(int argc, char** argv)
{
	// set up the sigaction behaviour
	struct sigaction *sig = malloc(sizeof(struct sigaction));
	sig->sa_flags = SA_RESTART | SA_NOCLDSTOP;
	
	// booleans
	int len = 0;
	int fileIn = 0;
	int fileOut = 0;
	int backgroundProcess = 0;
	char **args = NULL;

	while(1)
	{	// retrieve the input
		prompt();
		args = getln();
		set_flags(args, &len, &backgroundProcess, &fileIn, &fileOut);

		// force an iteration if no input was entered 
		if (len <=0) continue;
		if (backgroundProcess) 
		{
			args[len-1] = NULL; // remove the ampersand
			len--; // decrement len once
		}

		/* reset the sigaction behaviour */
		sig->sa_flags = SA_RESTART | SA_NOCLDSTOP;
		sig->sa_handler = SIG_IGN;
		sigaction(SIGCHLD, sig, NULL);

		// fork the process
		pid_t pid = fork();
		if (pid < 0) // child fork creation error
		{
			perror("fork");
			exit(EXIT_FAILURE);
		}
		else if (pid == 0) // child process 
		{
			int res = 0;
			// check for file input/ouput
			if (fileIn) input_from_file(len, args);
			else if (fileOut) output_to_file(len, args);			

			// check if args
			if (strcmp("gcd", args[0]) == 0)
			{
				gcd(len, args);
			} 
			else if (strcmp("args", args[0]) == 0) 
			{
				res = argument_counter(len, args);
			}
			else if (strcmp("pf", args[0]) == 0)
			{
				res = prime_factor(len, args);
			}
			else 
			{
				res = execvp(args[0], args);
			}
			if (res == -1)
			{
				fprintf(stderr, "ish: %s: ", args[0]);
				perror("");
				exit(EXIT_FAILURE); // exit out of child
			} 
			exit(EXIT_SUCCESS);
		}
		else // pid > 0, inside parent process 
		{
			// check if this is a background process
			if (!backgroundProcess) 
			{
				// wait for a specific child to break
				waitpid(pid, NULL, 0);
			}
			else // if not a background process
			{ // look for a flag indicating the termination of a charger
				sig->sa_flags = SA_RESTART | SA_NOCLDSTOP;
				sig->sa_handler = &sig_child_handler;
				sigaction(SIGCHLD, sig, NULL);
			}
		}
	}
	free(sigaction);
	return 0;
}

/*
*	Gets the user name and host name and creates a prompt based on it.
*/
void prompt()
{
	// get the UID
	struct passwd *pw;
	uid_t uid = getuid();
	pw = getpwuid(uid);

	// get the host name
	char hostName[1024];
	hostName[1023] = '\0';
	gethostname(hostName, 1023);

	// print the prompt depending on user levle
	if (uid) printf ("[%s@%s]$ ", pw->pw_name, hostName);
	else printf ("[%s@%s]# ", pw->pw_name, hostName);
}

/*
*	Custom handler to handle terminated background processes.
*	@param sigint: the signal passed (not used)
*/
void sig_child_handler(int sigint)
{
	wait (NULL);
	if (errno == EINTR)
	{
		errno = 0; // reset errnor on system call interrupt
	} 
}

/*
*	Sets the stdin to the file specified.
*	@param len: the number of arguments
*	@param args: the list of arguments
*/
void input_from_file(int len, char **args)
{
	freopen(args[len-2], "r", stdin);
	args[len-2] = args[len-1];
	args[len-1] = NULL;	
	
}

/*
*	Sets the stdout to the file specified.
*	@param len: the number of arguments
*	@param args: the list of arguments
*/
void output_to_file(int len, char **args)
{
	freopen(args[len-1], "w", stdout);
	args[len-1] = NULL;
	args[len-2] = NULL;
}

/*
*	Function that does all the initial flag setting and counting the number of arguments.
*	@param args: the list of arguments
*	@param len: the number of arguments
*	@param backgroundProcess: if an ampersand is the last arg
*	@param fileIn: if a "<" is the 2nd last arg, set this
*	@param fileOut: if a ">" is the 2nd last arg, set this
*/
void set_flags(char **args, int *len, int *backgroundProcess, int *fileIn, int *fileOut)
{
	// check and store the length
	for (*len = 0; args[*len] != NULL; (*len)++);

	if ((*len) <= 0) return;

	// determine if the user wants this to be a background process
	*backgroundProcess = 0;
	if (strcmp(args[(*len)-1], "&") == 0) 
	{
		*backgroundProcess = 1;
	}

	// check for arrow
	*fileIn = 0;
	*fileOut = 0;
	if (*len > 2)
	{
		if (strcmp(args[(*len)-2], "<") == 0) *fileIn = 1;
		else if (strcmp(args[(*len)-2], ">") == 0) *fileOut = 1;
		if (backgroundProcess && (*len) > 3)
		{
			if (strcmp(args[(*len)-3], "<") == 0) *fileIn = 1;
			else if (strcmp(args[(*len)-3], ">") == 0) *fileOut = 1;
		}
	}

	// check for exit command
	if (*len == 1 && strcmp("exit", args[0]) == 0) exit(EXIT_SUCCESS);
}

/*---------------------------------- COUNT ARGUMENTS ---------------------------------*/
/*
*	Function that counts the arguments passed.
*	@param argc: the number of arguments passed
*	@param argv: the list of arguments
*	@return: 0 if exit_sucess, -1 otherwise
*/
int argument_counter(int argc, char **argv)
{
	if (argc < 2)
    {
        fprintf(stderr, "usage: %s: arg1, [arg2, arg3, ...]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("argc: %d\targs: ", argc-1);
        int i;
        for (i = 1; i < argc; i++)
        {
            if (i == argc-1) printf("%s\n", argv[i]);
            else printf("%s, ", argv[i]);
        }
    }
    return EXIT_SUCCESS;
}

/*---------------------------------- CALCULATE GCD -----------------------------------*/
/*
*	Initial function that handles calling helper functions.
*	@param argc: the number of arguments passed
*	@param argv: the list of arguments
*	@return: 0 if exit_sucess, -1 otherwise
*/
int gcd(int argc, char **argv)
{
	if (argc != 3)
    {
        fprintf(stderr, "usage: %s: num1, num2 (arguments must be a valid decimal or hexadecimal).\n",argv[0]);
        return -1;
    }
    else
    {
		int error = 0;
        long num1 = parse_number(argv[1], &error);
        long num2 = parse_number(argv[2], &error);
		if (error == -1) return -1;
        long res = calculate_gcd(num1, num2);
        if (res < 0) res = -res;
        printf("GCD(%s,%s) = %ld\n", argv[1], argv[2], res);
    }
    return EXIT_SUCCESS; 
}

/*
*	Initial function that handles calling helper functions.
*	@param num1: the first number
* 	@param num2: the second number
*	@return: the gcd calculated
*/
long calculate_gcd(long num1, long num2)
{
    if (num2 == 0) return num1;
    else return calculate_gcd(num2, num1 % num2);
}

/*
*	Checks if the number entered does not contain letters and parses it into a number.
*	@param num: the number to check
* 	@param *error: pointer to set if an error occurs
*	@return: the parsed number
*/
long parse_number(char *num, int *error)
{
    char *string = NULL;
    int base = 10;

    // check if the base is 16 or not    
    if(num != NULL)
    {
        if (strlen(num) > 2 && num[1] == 'x') base = 16;
        else if (strlen(num) > 3 && num[2] == 'x') base = 16;
    }

    long ret = strtol(num, &string, base);
    if (string != NULL && strlen(string) > 0) 
    {
        fprintf(stderr, "Number entered was invalid.\n");
        *error = -1;
    }
    return ret;
}

/*----------------------------- CALCULATE PRIME FACTOR -------------------------------*/

// took the algorithm for calculating off geeksforgeeks.com
/*
*	Initial function to call that handles calling the helper functions.
*	@param argc: the number of arguments passed
*	@param argv: the list of arguments
*	@return: 0 if exit_sucess, -1 otherwise
*/
int prime_factor(int argc, char **argv)
{
	if (argc != 2)
    {
        fprintf(stderr, "usage: %s: num1 (argument must be a valid decimal integer).\n",argv[0]);
        return -1;
    }
    else
    {
		int error = 0;
        int num = check_number(argv[1], &error);
		if (error == -1) 
		{
			printf("Error: Not a number.\n");
			return -1;
		}
        calculate_factors(num);
    }
    return EXIT_SUCCESS; 
}

// took the algorithm for calculating off geeksforgeeks.com
/*
*	Calculate prime factors. The algorithm was created by 
*	geeksforgeeks. The full link is in the readme.
*	@param int n: the number to calculate the factors of
*/
void calculate_factors(int n)
{
	printf("The prime factors of %d are: ", n);
	// Check if divisible by 2, and print the number of times its divisible by 2
    while (n % 2 == 0) 
    { 
        printf("%d ", 2); 
        n = n/2; 
    } 
  
    // check every odd number that divides it
	int i = 3;
    for (i = 3; i <= sqrt(n); i = i+2) 
    { 
        // While i divides n, print i and divide n 
        while (n % i == 0) 
        { 
            printf("%d ", i); 
            n = n/i; 
        } 
    } 
  
    // if its a prime, but its greater than 2, print it
    if (n > 2) printf ("%d ", n); 
	printf("\n");
}

/*
*	Checks if the number entered does not contain letters.
*	@param num: the number to check
* 	@param *error: pointer to set if an error occurs
*	@return: the parsed number
*/
int check_number(char *num, int *error)
{
	char *string = NULL;
    int base = 10;

    long ret = strtol(num, &string, base);
    if (string != NULL && strlen(string) > 0) 
    {
        fprintf(stderr, "Number entered was invalid.\n");
        *error = -1;
    }
    return (int)ret;
}