void permute(char *permutation, char *word, int index, int end)
{
  int i;
  /* base case
  if the you already placed the last char in the permutation */
  if (index > end)
  {
    /* print the word + the current count
    use this if you want to see the numbering of all 720 permutation */
    /* printf("%s %d\n", permutation, count); */

    /* basic print without the count */
    printf("%s\n", permutation);
    /* exit out of the recurse */
    return;
  }
  /* this loops through every character in word */
  for (i = 0; i <= end; i++)
  {
    /* this make sure that the current index has a unique
    character by checking the current 'set' characters*/
    int repeat = 0;
    int j;
    for (j = index-1; j >= 0; j--)
    {
      /* if the current character has already appeared
      previously in the permutation, then its a repeat*/
      if (word[i] == permutation[j]) repeat = 1;
    }
    /* force the iteration of the char loop (outside loop)
    so that you can look at another character to place in the permutation */
    if (repeat) continue;
    else
    {
      permutation[index] = word[i]; /* place the character*/
      permute(permutation, word, index+1, end); /* permute it */
    }
  }
}

int main()
{
    
}