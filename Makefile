all: ish
ish: ish.o lex.yy.o
	gcc -o ish ish.o lex.yy.o -lfl -lm
ish.o: ish.c
	gcc -Wall -g -D_XOPEN_SOURCE=700 -c ish.c
lex.yy.o: lex.yy.c
	gcc -Wall -g -D_XOPEN_SOURCE=700 -c lex.yy.c
lex.yy.c: lex.c
	flex lex.c
git:*.c
	git init
	git add Makefile
	git add *.c
	git add readme.txt
	git commit -m "backup"
	git remote rm origin
#	git config credential.helper store
#	git config --global credential.helper 'cache --timeout 3600'
	git remote add origin http://github.com/SeegalPanchal/TempSave.git
	git push -u origin master
clean:
	rm -f *.o
	rm -f lex.yy.c
	rm -f ish
	rm -f gcd
	rm -f args

