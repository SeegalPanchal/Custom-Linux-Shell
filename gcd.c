// Seegal Panchal
// Feb 6th 2018
// This function checks the GCD of two numbers inputted
// they can be either decimal or hexadecimal

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long parseNumber(char *num)
{
    char *string = NULL;
    int base = 10;

    // check if the base is 16 or not    
    if(num != NULL)
    {
        if (strlen(num) > 2 && num[1] == 'x') base = 16;
        else if (strlen(num) > 3 && num[2] == 'x') base = 16;
    }

    long ret = strtol(num, &string, base);
    if (string != NULL && strlen(string) > 0) 
    {
        fprintf(stderr, "Number entered was invalid.\n");
        exit(EXIT_FAILURE);
    }
    return ret;
}

long calculate(long num1, long num2)
{
    if (num2 == 0) return num1;
    else return calculate(num2, num1 % num2);
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "usage:%s,num1,num2, arguments must be a valid decimal or hexadecimal\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    else
    {
        long num1 = parseNumber(argv[1]);
        long num2 = parseNumber(argv[2]);
        long res = calculate(num1, num2);
        if (res < 0) res = -res;
        printf("GCD(%s,%s) = %ld\n", argv[1], argv[2], res);
    }
    return EXIT_SUCCESS; 
}