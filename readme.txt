Name: Seegal Panchal
ID: 1016249
Date: Feb 7th 2018

Note: The warning on initial make are just because of the auto generated lex.yy.c file
        and not because of my program.

Note: If you type command followed by ampersand, 
        the prompt prints early if the background process ends too fast.
        It prints the prompt followed bythe output. 
        You can still type another command, but you can 
        also hit enter again to show the prompt.
        e.g.
        [oscreader@OSC]$ xemacs &
        [oscreader@OSC]$ ish: xemacs: No such file or directory
        |

        ^^ the bar represents the cursor, hitting enter does the following:
        [oscreader@OSC]$ xemacs &
        [oscreader@OSC]$ ish: xemacs: No such file or directory
        
        [oscreader@OSC]$ |

HOW TO RUN:
1.0: Type make.
        There might be errors here before is used #define _XOPEN_SOURCE 700, but sometimes it doesn't work
        and you need to change the makefile.

2.1: [SHELL PROMPT] Should just be on the left.
2.2 and 2.3: [RUN COMMANDS WITH ARGUMENTS] Just enter commands, like "ls -l", etc...
2.4: [BACKGROUND PROCESS]: Type ampersand as the last argument. 
                            If its last argument, remove, and don't call wait instantly.
                            Note: I used sigaction() instead of sigset()
2.5: [OUTPUT REDIRECTION]: The arrow and file name MUST be the last two commands
                            e.g. input [oscreader@OSC]$ ls -l > foo
                            // this will paste the output of 'ls -l' in 'foo'
2.6: [INPUT REDIRECTION]: The arrow and file name MUST be the last two commands
                            e.g. input [oscreader@OSC]$ sort < foo
                            // this will paste the output of 'ls -l' in 'foo'

Note: input/output redirection works in the background as well if you put an &.

3.1: [gcd]: type 'gcd' and enter two numbers and it will return the gcd.
            Assumption: all hex input as a '0x' in front of it
            e.g. input: [oscreader@OSC]$ gcd 25 15
                 output: GCD(25,15) = 5
3.2: [args]: type 'args' and enter any number of arguments, it will count and 
            return the count of arguments and arguments themselves
            e.g. input: [oscreader@OSC]$ args 1 2 3 4 5 "six seven" eight
                 output: argc = 7 args = 1, 2, 3, 4, 5, "six seven", eight
3.3: [pf] type 'pf' followed my a number to see its prime factors (the algorithm is from geeksforgeeks, link below)
            Assumption: input is integer.
            e.g. input [oscreader@OSC]$ pf 315
                 output: The prime factors of 315 are: 3 3 5 7


----------------------- IN DEPTH EXPLANATION (MAINLY INTO THE SIGNALS CAUSE THIS TOOK FOREVER) -----------------

SHELL PROMPT:
--> The shell name is found through using the passwd struct. The struct is initialized by calling it on
    getuid(). I can then simply use the passwd struct to access the user name.
    The host name is simpler as you can just use gethostname();

ALLOWS COMMANDS WITH ARGUMENTS TO RUN IN THE SHELL:
--> By parsing input using the lex file, we can easily pass 
    the return of getln() to execvp() and it handles arguments and errors.

BACKGROUND PROCESSES - SOLVING SIGNALS AND ZOMBIE PROCESSES:
--> After reading these links, and yes it took that long, I came up with the following
    conditions which lead to solutions I used later:
        1. Wait is used to retrieve a process from the process table.
        2. When you run a process, and the process terminates, the process needs 
            to be waited on by the parent process using Wait() or Waitpid().
        3. When a process terminates, it sends a SIGCHLD signal indicating the 
            the child process has terminated. 
        4. Wait() and Waitpid() clear the process from the process table. Thus, we can
            this to our advantage to clear the background child process from the process table.
        5. However, during my implementation, the waits and signals kept getting mixed up.
            I was able to solve it at one point using sigset, but when the VM restarted
            it stopped working. The sigset solution is just replacing SIGACTION with sigset
            and using errno to check for system call interrupt rather than using the SA_RESTART
            flag.
--> Solution: My program follows this process:
        1. I create a sigaction() struct which holds certain behaviour (the flags and 
            the custom handler.) The flags I used are as follows:
            - SA_RESTART: this is needed because when reading the SIGCHLD function
                            the process causes an error because it interrupts a background 
                            system call. SA_RESTART auto restarts the call that was interrupted.
                            THIS WAS EXTREMELY IMPORTANT BECAUSE CALLING A WAIT CAUSES A LEX
                            INPUT ERROR OTHERWISE.
            - SA_NOCLDSTOP: this is needed so that any signal that was released that stopped or   
                            continued a child process doesn't interrupt the program
        2. Initially, I set the SIGCHLD signal to be ignored. This is only changed if the
            process is a background process.
        3. If it is a background process, I wait for the SIGCHLD signal that shows the child
            was terminated, and call a wait in the handler function to clear the zombie.
            You can check if it was a zombie by typing ps -a.
- https://docs.oracle.com/cd/E19455-01/806-4750/signals-7/index.html
- http://man7.org/linux/man-pages/man7/signal.7.html
- http://man7.org/linux/man-pages/man3/sigset.3.html
- https://www.geeksforgeeks.org/signals-c-language/
- https://www.geeksforgeeks.org/signals-c-set-2/
- http://man7.org/linux/man-pages/man2/kill.2.html
- https://linux.die.net/man/2/waitpid
- https://www.ibm.com/support/knowledgecenter/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpxbd00/rtsigac.htm?view=embed#rtsigac__trs1
- https://stackoverflow.com/questions/28457525/how-do-you-kill-zombie-process-using-wait
- https://stackoverflow.com/questions/5113545/enable-a-signal-handler-using-sigaction-in-c
- https://stackoverflow.com/questions/28457525/how-do-you-kill-zombie-process-using-wait
- https://stackoverflow.com/questions/16944886/how-to-kill-zombie-process
- https://stackoverflow.com/questions/43155640/signal-handling-for-background-processes

INPUT/OUTPUT REDIRECTION:
--> By taking freopen and using it according to the man page (associating the correct input/output stream),
    any change in the stream state we can send the changes to the desired file using just execvp.
    Calling freopen with stdin allows you to redirect input, so the input stream points to the FILE.
    Calling freopen with stdout allowed you to redirect output to a file.

GREATEST COMMON DIVISOR:
--> I simply check if 'gcd' was called and call my custom function
    I used euclids algorithm to calculate a GCD.
    Source: https://www.geeksforgeeks.org/euclidean-algorithms-basic-and-extended/

ARGUMENT COUNTER:
--> Just like GCD, I call it my argument counter custom function of the user entered 'gcd'
    This was really easy because I added a regex to my readme which accounts for quotations
    marks and single quotes turning it into a single argument rather than two. So we can just
    count the number of arguments that were passed and thats the entire funtion right there.

CUSTOM, PRIME FACTORS: TYPE 'pf' and pass a number to determine its prime factors:
--> Similar to GCD and Args, this was simply called if the user called 'pf <num>'.
I used the prime factor formula from geeks for geeks:
https://www.geeksforgeeks.org/print-all-prime-factors-of-a-given-number/